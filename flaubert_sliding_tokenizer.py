import sys

import torch

from transformers import FlaubertModel, FlaubertTokenizer

from operator import add

from pprint import pprint

from dynprogalign import indexed_object

# Choose among ['flaubert/flaubert_small_cased', 'flaubert/flaubert_base_uncased', 
#               'flaubert/flaubert_base_cased', 'flaubert/flaubert_large_cased']
# pap
#model_name = 'flaubert/flaubert_base_cased' 
default_model_name = 'flaubert/flaubert_small_cased'

model_encoded_input_repr_size = { 'flaubert/flaubert_small_cased'  : 512,
                                  'flaubert/flaubert_base_uncased' : 768,
                                  'flaubert/flaubert_base_cased'   : 768,
                                  'flaubert/flaubert_large_cased'  : 1024 }

#DEBUG=True
DEBUG=False

def to_list_of_indexed_objects( embedded_tokens ):
     # transform a list of pairs < word_form, embedding_list > into a list of indexed object
     res = []
     for i in range( 0, len( embedded_tokens )):
          if DEBUG:
               print( 'to_list_of_indexed_objects() i == {0}'.format( i ))
          res.append( indexed_object( myrepr = embedded_tokens[ i ][ 0 ],
                                      idx = i,
                                      annotations = { 'BERT_Representation' : embedded_tokens[ i ][ 1 ] } ))
     if DEBUG:
          print( 'to_list_of_indexed_objects() result is == {0}'.format( res[0:10] ))
     return res

def any_text_len_flaubertizer( the_text = '', model_name = default_model_name ):
     # for a given text returns a torch.Tensor of shape (1, N, model_max_input_sz )
     # where N is the number of flaubert token embeddings

     if DEBUG:
          print( 'any_text_len_flaubertizer the_text== {0}'.format( the_text ))
          sys.stdout.flush()
     try:
          model_max_input_sz = model_encoded_input_repr_size[ default_model_name ]
     except exception as e:
          print( 'BUG exception raised for model== {0}'.format( e ))
          sys.stdout.flush()
          assert( 0 )
     if DEBUG:
          print( 'FLAUBERT A0')
          sys.stdout.flush()
     flaubert, log = FlaubertModel.from_pretrained( model_name, output_loading_info=True )
     if DEBUG:
          print( 'FLAUBERT A1')
          sys.stdout.flush()
     flaubert_tokenizer = FlaubertTokenizer.from_pretrained( model_name, do_lowercase=False )
     if DEBUG:
          print( 'FLAUBERT A2')
          sys.stdout.flush()

     token_lst = flaubert_tokenizer._tokenize( the_text )
     if DEBUG:
          print( 'FLAUBERT A3')
          sys.stdout.flush()
     encoded_tokens = [ flaubert_tokenizer.encode( the_text ) ]
     if DEBUG:
          print( 'FLAUBERT A4')
          sys.stdout.flush()
     token_ids = torch.tensor( encoded_tokens )
     if DEBUG:
          print( 'FLAUBERT A5')
          sys.stdout.flush()
##     print( 'token_ids.shape[1]=={0}'.format( token_ids.shape[1] ))
     lentokids = token_ids.shape[1]
     if DEBUG:
          print( 'FLAUBERT A6')
          sys.stdout.flush()
     win_sz = model_max_input_sz
     res_embedding_lst = []
     if DEBUG:
          print( 'FLAUBERT A7')
          sys.stdout.flush()
     chunk0_ids = token_ids[ 0 :  min( win_sz, lentokids) ] ; chunk1_ids = []
     if DEBUG:
          print( 'FLAUBERT A8')
          sys.stdout.flush()
     if DEBUG:
          print( 'lentokids == {0}'.format( lentokids ))
          sys.stdout.flush()

     if lentokids > model_max_input_sz:
          if DEBUG:
               print( 'lentokids > {0}'.format( model_max_input_sz ))
               sys.stdout.flush()
               
          ratio = int( lentokids / model_max_input_sz ) + 1
          assert( ratio > 1 )
          half_win_sz = int( win_sz / 2.0)

          if DEBUG:
               print( 'FLAUBERT A9')
               sys.stdout.flush()
          
          for winidx in range( 0, lentokids, win_sz ):
               if DEBUG:
                    print( 'FLAUBERT B winidx== {0}'.format( winidx ))
                    sys.stdout.flush()
               if winidx + half_win_sz < lentokids:
                    if DEBUG:
                         print( 'FLAUBERT C' )
                         print( 'winidx + half_win_sz == {0} lentokids== {1}'.format( winidx + half_win_sz, lentokids ))
                         sys.stdout.flush()
                    # compute chunk0 embeddings, at current sliding window position
                    chunk0_ids  = token_ids[  : , winidx : (winidx + win_sz) ]
                    # the current sliding window position is full (i.e. we are not processing the last window of the for loop)
                    # it also means that there are some token beyond the end of the current slideing window,
                    # so neither the second half of the overlap and chunk1 will be empty. 
                    chunk0_last_layer = flaubert( chunk0_ids )[ 0 ]
                    first_half_chunk0_last_layer  = chunk0_last_layer[ : , 0:half_win_sz , : ]
                    second_half_chunk0_last_layer = chunk0_last_layer[ : , half_win_sz:  , : ]
               
                    overlap_tok_ids = token_ids[  : , (winidx + half_win_sz) : min( (winidx + win_sz + half_win_sz), lentokids)  ]
                    overlap_last_layer = flaubert( overlap_tok_ids )[0]
                    # since chunk0 is filled-up, the first half of the overlap (that start a half sliding window size after chunk0) is alo full.
                    firts_half_overlap_last_layer  = overlap_last_layer[ : , 0:min( half_win_sz, overlap_last_layer.shape[ 1 ]) , : ]
                    # for the second half of the overlap, it can be either full or partially filled up.
                    second_half_overlap_last_layer = overlap_last_layer[ : , min( half_win_sz, overlap_last_layer.shape[ 1 ]):  , : ]

                    chunk1_ids  = token_ids[  : , (winidx + win_sz) : min( lentokids, (winidx + (2 *win_sz))) ]
                    chunk1_last_layer = flaubert( chunk1_ids )[0]
                    first_half_chunk1_last_layer  = chunk1_last_layer[ : , 0:half_win_sz , : ]
                    if (winidx + win_sz + half_win_sz) < lentokids:
                         second_half_chunk1_last_layer = chunk1_last_layer[ : , half_win_sz:  , : ]
                    else:
                         second_half_chunk1_last_layer = None
                    # a) combine (average) the overlap with the content of chunk0 and chunk1 in the following way:
                    # - add: first_half_overlap_last_layer and second_half_chunk0_last_layer into res1
                    # - add: second_half_overlap_last_layer and first_half_chunk1_last_layer into res2
                    # - concatenate into the resulting first chunk:  first_half_chunk0_last_layer and res1
                    # - concatenate into the resulting second chunk: res2 and second_half_chunk1_last_layer (if it exists)
                    # b) accumulate the first chunk into the final results structure
                    # c) transfert the content of the second chunk to the first chunk
                    first_half_overlap_average     = (firts_half_overlap_last_layer  + second_half_chunk0_last_layer ) / 2.0
                    if second_half_chunk1_last_layer is None:
                         second_half_overlap_average = second_half_overlap_last_layer
                    else:
                         second_half_overlap_average = (second_half_overlap_last_layer + first_half_chunk1_last_layer ) / 2.0
                    res_chunk0 = torch.cat( (first_half_chunk0_last_layer, first_half_overlap_average), dim=1 )
                    assert( res_chunk0.shape[ 1 ] == win_sz )
                    if second_half_chunk1_last_layer is None:
                         res_chunk1 = second_half_overlap_average
                    else:
                         res_chunk1 = torch.cat( (second_half_overlap_average, second_half_chunk1_last_layer), dim=1 )
                    assert( res_chunk1.shape[ 1 ] <= win_sz )
                    res_embedding_lst.append( res_chunk0 )
                    res_chunk0 = res_chunk1
               else:
                    assert( (winidx + win_sz) >= lentokids )
                    if DEBUG:
                         print( 'FLAUBERT D' )
                         print( 'winidx + half_win_sz == {0} lentokids== {1}'.format( winidx + half_win_sz, lentokids ))
                         sys.stdout.flush()
                    res_embedding_lst.append( res_chunk0 )
          res_embeddings = torch.cat( tuple( res_embedding_lst ), dim=1 )
          if DEBUG:
               print( 'lentokids== {0} and all the len( res_embedding_lst )== {1} res_embeddings.shape== {2}'.format( lentokids,
                                                                                                                 [ x.shape[ 1 ] for x in res_embedding_lst], 
                                                                                                                 res_embeddings.shape ) )
               sys.stdout.flush()
          assert( res_embeddings.shape[ 1 ] == lentokids )
          if DEBUG:
               print( 'FLAUBERT E' )
               sys.stdout.flush()
     else:
          assert( lentokids <= model_max_input_sz )
          res_embeddings = flaubert( chunk0_ids )[0]
     if DEBUG:
          print( '>>>>>>>res_embeddings == {0}'.format( res_embeddings ))
          sys.stdout.flush()
     aux_res_embeddings_lst = res_embeddings[0].tolist()
     if DEBUG:
          print( '!!!!!!!len(aux_res_embeddings_lst)== {0} aux_res_embeddings_lst == {1}'.format( len( aux_res_embeddings_lst),
                                                                                           aux_res_embeddings_lst[0:10] ))
          sys.stdout.flush()
     aux_res_token_lst = list( zip( [ x.replace( '</w>', '' ) for x in token_lst ], aux_res_embeddings_lst ))
     if DEBUG:
          print( '!!!!!!len( aux_res_token_lst== {0} aux_res_token_lst == {1}'.format( len(aux_res_token_lst), aux_res_token_lst[0:10] ) )
          sys.stdout.flush()
     return to_list_of_indexed_objects( aux_res_token_lst )

#---- some data for testing

##ddhc_512words = "Les représentants du peuple français, constitués en Assemblée nationale, considérant que l'ignorance, l'oubli ou le mépris des droits de l'homme sont les seules causes des malheurs publics et de la corruption des gouvernements, ont résolu d'exposer, dans une Déclaration solennelle, les droits naturels, inaliénables et sacrés de l'homme, afin que cette Déclaration, constamment présente à tous les membres du corps social, leur rappelle sans cesse leurs droits et leurs devoirs; afin que les actes du pouvoir législatif, et ceux du pouvoir exécutif pouvant à chaque instant être comparés avec le but de toute institution politique, en soient plus respectés; afin que les réclamations des citoyens, fondées désormais sur des principes simples et incontestables, tournent toujours au maintien de la Constitution et au bonheur de tous. \n\nEn conséquence, l'Assemblée nationale reconnaît et déclare, en présence et sous les auspices de l'Être suprême, les droits suivants de l'homme et du citoyen: \n\nArticle premier - Les hommes naissent et demeurent libres et égaux en droits. Les distinctions sociales ne peuvent être fondées que sur l'utilité commune. \n\nArticle II - Le but de toute association politique est la conservation des droits naturels et imprescriptibles de l'homme. Ces droits sont la liberté, la propriété, la sûreté, et la résistance à l'oppression. \n\nArticle III - Le principe de toute souveraineté réside essentiellement dans la nation. Nul corps, nul individu ne peut exercer d'autorité qui n'en émane expressément. \n\nArticle IV - La liberté consiste à faire tout ce qui ne nuit pas à autrui: ainsi l'exercice des droits naturels de chaque homme n'a de bornes que celles qui assurent aux autres membres de la société la jouissance de ces mêmes droits. Ces bornes ne peuvent être déterminées que par la loi. \n\nArticle V - La loi n'a le droit de défendre que les actions nuisibles à la société. Tout ce qui n'est pas défendu par la loi ne peut être empêché, et nul ne peut être contraint à faire ce qu'elle n'ordonne pas. \n\nArticle VI - La loi est l'expression de la volonté générale. Tous les citoyens ont droit de concourir personnellement, ou par leurs représentants, à sa formation. Elle doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse. Tous les citoyens, étant égaux à ses yeux, sont également admissibles à toutes dignités, places et emplois publics, selon leurs capacités et sans autre distinction que celle de leurs vertus et de leurs talents. \n\nArticle VII - Nul homme ne peut être accusé, arrêté ni détenu que dans les cas déterminés par la loi, et selon les formes qu'elle a prescrites. Ceux qui sollicitent, expédient, exécutent ou font exécuter des ordres arbitraires, doivent être punis; mais tout citoyen appelé ou saisi en vertu de la loi doit obéir à l'instant; il se rend coupable par la résistance. \n\nArticle VIII - La loi ne doit établir que des peines strictement et évidemment nécessaires, et nul ne peut être puni qu'en vertu d'une loi établie et promulguée antérieurement au délit et légalement appliquée. \n\nArticle IX - Tout homme étant présumé innocent jusqu'à ce qu'il ait été déclaré coupable, s'il\n"
##
##embedded_token_lst = any_text_len_flaubertizer( the_text = ddhc_512words, model_name = 'flaubert/flaubert_small_cased' )
##
##pprint( [ '{0}'.format( x ) + '/' + '{0}'.format( x.annotations[ 'BERT_Representation' ] ) for x in embedded_token_lst[ 0 : 3 ] ])

##pprint( [ (x, len(y)) for x,y in embedded_token_lst[0:5] ] )
##
##pprint( [ (x, y[0:7]) for x,y in embedded_token_lst[0:5] ] )

##======================== RESTART: /home/pap/MAPA/TOOLS/python/map_reduce_stanza_bert/flaubert_sliding_tokenizer.py ========================
##Some weights of the model checkpoint at flaubert/flaubert_small_cased were not used when initializing FlaubertModel: ['pred_layer.proj.weight', 'pred_layer.proj.bias']
##- This IS expected if you are initializing FlaubertModel from the checkpoint of a model trained on another task or with another architecture (e.g. initializing a BertForSequenceClassification model from a BertForPreTraining model).
##- This IS NOT expected if you are initializing FlaubertModel from the checkpoint of a model that you expect to be exactly identical (initializing a BertForSequenceClassification model from a BertForSequenceClassification model).
##Token indices sequence length is longer than the specified maximum sequence length for this model (612 > 512). Running this sequence through the model will result in indexing errors
##winidx == 0
##lentokids== 612 and all the len( res_embedding_lst )== [512, 100] res_embeddings.shape== torch.Size([1, 612, 512])
##[('Les</w>', 512),
## ('représentants</w>', 512),
## ('du</w>', 512),
## ('peuple</w>', 512),
## ('français</w>', 512)]
##[('Les</w>',
##  [-0.16101600229740143,
##   -1.1228914260864258,
##   0.49461525678634644,
##   -2.086954355239868,
##   -0.3768198788166046,
##   -1.7505403757095337,
##   -0.9303699135780334]),
## ('représentants</w>',
##  [-0.32467660307884216,
##   2.6953606605529785,
##   0.9932524561882019,
##   0.38554874062538147,
##   -0.3591357469558716,
##   0.9991164207458496,
##   2.322113037109375]),
## ('du</w>',
##  [-1.6885340213775635,
##   1.3396852016448975,
##   1.6592336893081665,
##   -0.24301448464393616,
##   -0.5075089931488037,
##   2.369213581085205,
##   1.148726463317871]),
## ('peuple</w>',
##  [-0.7329034209251404,
##   -1.8499172925949097,
##   3.9860384464263916,
##   -0.7597928047180176,
##   -0.8464579582214355,
##   2.3789989948272705,
##   -2.6898844242095947]),
## ('français</w>',
##  [-1.475467562675476,
##   0.10895499587059021,
##   -0.5141370296478271,
##   0.9432658553123474,
##   -1.4878195524215698,
##   1.3563110828399658,
##   1.419601559638977])]
##>>> 
