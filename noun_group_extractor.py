# copyright Creative Commons:  CC BY NC SA
# author Patrick Paroubek, pap@lisn.fr
# date April 05 2022

import sys
from text_span_v2 import mwu, relation, construction, document, name
from unification import *
from unification.match import *
from pprint import pprint
import itertools
from copy import deepcopy

#DEBUG = True
DEBUG = False

def mwu_is_src_of_rel_typs( m, reltyps, rels ):
    for r in rels:
        if (r.typ in reltyps) and (r.src == m):
            return True
    return False
   
def pattern_filter_noun_group( doc, exclude_determiner_p = True ):
    STANZA_GN_SEED_RELS_EXCLUSION_LST     = [ 'compound', 'compound:prt', 'flat', 'goeswith', 'name' ]
    STANZA_GN_SUBTREE_RELS_EXCLUSION_LST  = [ 'cc', 'cc:preconj', 'conj', 'sconj', 'punct', 'mark', 'appos', 'nmod', 'acl:relcl', 'obl', 'aux',
                                              'punct', 'nsubj', 'csubj', 'nsubjpass',
                                              'csubjpass', 'obj', 'ccomp', 'xcomp', 'iobj', 'nmod:tmod', 'foreign', 'acl', 'acl:recl', 'appos',
                                              'neg', 'case', 'list', 'dislocated', 'parataxis', 'orphan', 'reparandum', 'dep', 'root',
                                              'vocative', 'discourse', 'expl', 'aux', 'aux:pass', 'cop' ]
    if exclude_determiner_p:
        STANZA_GN_SUBTREE_RELS_EXCLUSION_LST += [ 'det', 'det:predet' ]
    # we want to consider in the nominal subtree the following dependencies:
    # ---------relations where the target is either the core noun or an outer border word
    # compound, compound:prt, flat, goeswith
    # amod, advmod (when modifying an adjective),
    # conj (when the source and the head are adjectives or adverbs),
    #-------------------------
    # NOTE: This cases below are not handled for now
    # 'nmod:poss' only when the source is a possessive determiner
    # --------- relations where the source is either the core noun or an outter border word
    ## 'nmod:npmod' only when the source is a noun (exclude pronouns)
    ## NOTE: stanza annotates nmod:npmod as obl:npmod
    # 'nummod' 
    ## 'name' relation seems to exist in udped, is it produced by stanza ?
    #-----------

    # works with Stanza parser
    if DEBUG:
        print( 'processing doc== {0}'.format( doc.id ))
    # ---- step0 finding all noun words
    # select all words with upos NOUN
    noun_mwu_lst = []
    for (mwid, mw) in doc.mwus.items():
        #print( 'mw.annotations.keys() == {0}'.format( list(mw.annotations.keys()) ))
        msg = 'mw.annotations.values() == {0}'.format( list(mw.annotations.values()))
        # print( msg )
        if DEBUG:
            print( msg[0:min(200, len( msg ))] )
        if  mw.annotations[ 'w_upos' ] in [ ['NOUN'], ['PROPN'] ]:
            noun_mwu_lst.append( mw )
    noun_mwu_lst_nm = [ x.name for x in noun_mwu_lst ]

    # remove all mwus which are source from a compound dependency
    core_noun_mwu_lst = []
    for (rid, r ) in doc.rels.items():
        if (r.src.name in noun_mwu_lst_nm) and (r.typ in [ 'compound' ]):
            pass
        else:
            if r.trgt.name in noun_mwu_lst_nm:
                core_mwu = doc.mwus[ r.trgt.name ]
                if core_mwu not in core_noun_mwu_lst:
                    core_noun_mwu_lst.append( core_mwu )
    
    if DEBUG:
        print( 'BEFORE expanding SUBTREES  noun word list ==  {0}'.format( [ x.text( doc ) for x in noun_mwu_lst ] ))

    # extract the dependency subtree associated to each core noun mwu and 
    # create one construction for each noun group seed word
    noun_group_res_lst = []  # one pattern is created associated to each noun token found previously
    
    for m in core_noun_mwu_lst:
        depth = 0
        if not mwu_is_src_of_rel_typs( m, STANZA_GN_SEED_RELS_EXCLUSION_LST, doc.rels.values() ):
            # NOTE: better to always initialize explicitely default parameter values in the call to avoid sometimes strange side effects.
            m_kstruct = construction( nm = 'noun_subtree_' + name( m ), mwus = {}, rels = {}, opt_mwus = {}, opt_rels = {} )
            m_kstruct.add_mwu( m )
            if DEBUG:
                print( '>>>ADDING CORE MWU: {0} with construction {1}'.format( m, name( m_kstruct ) ) )
                print( 'm_kstruct.keys()=={0}'.format( m_kstruct.rels ) )
            outer_border = [] ; new_outer_border = []
            for r in doc.rels.values():
                if (r.typ not in STANZA_GN_SUBTREE_RELS_EXCLUSION_LST ) and (r.trgt == m):
                    # 'nmod:poss' only when the source is a possessive pronoun
                    # 'nmod:npmod' only when the source is a noun (exclude pronouns)
                    if DEBUG:
                        print( '>>>>>>>>ADDING OUTER_BODER relation {0}'.format( r ))
                    outer_border.append( r )
                    m_kstruct.add_mwu( r.src )
                    m_kstruct.add_rel( r )
            new_outer_border = []
            while outer_border:
                if DEBUG:
                    print( 'depth=={0}'.format( depth ))
                    print( 'outer_border== {0}'.format( outer_border ))
                    print( 'new_outer_border== {0}'.format( new_outer_border ))
                    print( '===========')
                for r_out in outer_border:
                    for r in doc.rels.values():
                        if (r.typ not in STANZA_GN_SUBTREE_RELS_EXCLUSION_LST ) and (r.trgt == r_out.src):
                            # 'nmod:poss' only when the source is a possessive pronoun
                            # 'nmod:npmod' only when the source is a noun (exclude pronouns)
                            if DEBUG:
                                print( '>>>>>>>>ADDING NEW OUTER_BODER relation {0}'.format( r ))
                            new_outer_border.append( r )
                            m_kstruct.add_mwu( r.src )
                            m_kstruct.add_rel( r )
                outer_border = new_outer_border  # pop the outer_border old level and append the new level
                new_outer_border = []
                depth += 1
                
            #------

            if DEBUG:
                print( 'A_____________________\n')
                print( '\n SUBTREE for mwu== {0} with text=={1} is:\n'.format( name( m ), m.text( doc ) ))
                print_rels( m_kstruct.rels )
                print( 'B_____________________\n')

            noun_group_res_lst.append( m_kstruct )
    if DEBUG:
        print( 'noun_group_res_lst is=={0}'.format( noun_group_res_lst ))
    return noun_group_res_lst


def print_rels( rels ):
    print( '========================= rels =============' )
    for (rnm, r) in rels.items():
        print( 'relation name={0} type={1} src={2}  {3} ==> trgt={4}  {5}'.format( r.name,
                                                                                   r.typ,
                                                                                   r.src.name,
                                                                                   r.src.annotations ['w_text'],
                                                                                   r.trgt.name,
                                                                                   r.trgt.annotations ['w_text'] ))
    print( '===================================================' )
    
def print_mwus( mwus, doc ):
    print( '========================= mwus =============' )
    for (mnm, m) in mwus.items():
        #print( 'mwu text== {0} name={1} type={2} annots= {3}'.format( m.text( doc ), m.name, m.typ, m.annotations[ 'w_upos' ] ))
        print( 'mwu text== {0} w_upos== {1}'.format( m.text( doc ), m.annotations[ 'w_upos' ] ))
    print( '===================================================' )

def filter_noun_groups( doc, exclude_determiner_p = True ):
    # subject verb object / attribute and their modifiers extraction
    # kstruct_lst = pattern_filter_svo_O_ADJ_triplet( list( doc.rels.values() ) )
    constructions_res = pattern_filter_noun_group( doc, exclude_determiner_p )

    if DEBUG:
        for k in constructions_res:
            msg = '++++++++++++++++++++++++++ noun groups filtering result for'
            msg += 'file {0} has found mwus {1} match(es) and rels {2} match(es): '.format( doc.id, len( k.mwus.keys()), len( k.rels.keys() ) )
            print( msg )
            print_mwus( k.mwus, doc )
            print_rels( k.rels )
            print( '===========' )
    return constructions_res

