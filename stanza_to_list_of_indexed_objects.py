## This code written in 2020 by Maha BOUZAIENE <bouzaiene.maha@gmail.com> and
## Patrick Paroubek (pap@limsi.fr) is licensed under CC BY-NC-SA.
## This license allows reusers to distribute, remix, adapt, and build upon the material in any medium
## or format for noncommercial purposes only, and only so long as attribution is given to the creator.
## If you remix, adapt, or build upon the material, you must license the modified material under
## identical terms. 
## CC BY-NC-SA includes the following elements:
## BY – Credit must be given to the creator
## NC – Only noncommercial uses of the work are permitted
## SA – Adaptations must be shared under the same terms
## For further information see https://creativecommons.org/about/cclicenses/
## or look into the directory: ./copyright_NOTICES


'''converts the stantza output associated to a text document into a list of indexed objects
with the stanza annotations represented as multiwords units and relations'''

from dynprogalign import indexed_object

import os
import sys
import io

from pprint import pprint 
import stanza

from pathlib import Path
from configparser import ConfigParser
conf_parser = ConfigParser( allow_no_value = True )
conf_file = Path( __file__ ).parent / "config.ini"
conf_parser.read( conf_file )

input_directory = conf_parser.get( 'configuration', 'input_dir' )
sufx = conf_parser.get( 'configuration', 'txt_sufx' )

class Stanza_Output( object ):
    
  def __init__( self, lang = 'en', srcdir = input_directory,  file_lst = [], sufx = sufx ):
    #----------
    assert( lang in [ 'en', 'fr' ] ) # for now we address only English and French
    assert( Path.is_dir( srcdir )  )
    assert( (type( file_lst ) is list) and file_lst and (not [ b for b in map( lambda x : type( x ) is str, file_lst) if b is False ]) )
    assert( type( sufx ) is str )
    #----------
    self.lang = lang
    self.nlp = stanza.Pipeline( lang, processors = 'tokenize,pos,lemma,depparse')
    self.document_dir = srcdir
    self.file_lst = file_lst
    self.txt_sufx = sufx
    self.curr_file_idx = 0
    self.curr_sent_idx = 0
    self.curr_tok_idx =  0
    self.n_mwu = 0
    with open( (srcdir / file_lst[ 0 ]).with_suffix( self.txt_sufx ), 'r', encoding='utf-8' ) as fdesc: 
      self.curr_text = fdesc.read()
      fdesc.close()
    self.curr_doc = self.nlp( self.curr_text )
    #   print( 'stanza loaded file {0}'.format( self.file_lst[ self.curr_file_idx ] ))
    #----------

  def next_tok( self ):
    # stop returning token at each document boundary (i.e. file boundary)
    if self.curr_sent_idx < len( self.curr_doc.sentences):
        if self.curr_tok_idx < len( self.curr_doc.sentences[ self.curr_sent_idx ].tokens ):
            tok = self.curr_doc.sentences[ self.curr_sent_idx ].tokens[ self.curr_tok_idx ]
            # cf https://stanfordnlp.github.io/stanza/mwt.html#accessing-parent-token-for-word
            # a token can generate several words with multi-word-token processing (mwt) activated, e.g. in French 'du' --> de+le
            # https://stanfordnlp.github.io/stanza/pos.html
            # when a token has several words associated, they can be accesses through: tk.words
            # and similarly, each word can access its parent token w.parent, in case of a multi-word token, the subsequent words have the same token as parent.
            # NOTE: I could do without 'id':tok.id and 'text':tok.text but for now I keep them, to be able to do some data integrity checking
            assert( (len( tok.id )== 1) or (len( tok.id ) == 2) )
            tokid = tok.id[0]

            if len( tok.id ) == 2:
            #   print( 'DEBUG tok.id== {0}'.format( tok.id ))
            #   # for multiword tokens
              assert( len( tok.words ) == len( tok.id ) )

            annots = { 'source':'stanza', 'id':tokid, 'sent_id':(self.curr_sent_idx + 1), 'text':tok.text, 'w_id':[], 'w_text':[],
                    'w_lemma':[], 'w_upos':[], 'w_xpos':[], 'w_feats':[], 'w_head':[], 'w_deprel':[], 'start_char':tok.start_char, 'end_char':tok.end_char }
            # note in stanza CoNLL format sentence numbering start at 1 (like the CoNLL token numbering)
            for w in tok.words:
                annots[ 'w_id'     ].append( w.id    )
                annots[ 'w_text'   ].append( w.text  )
                annots[ 'w_lemma'  ].append( w.lemma ) 
                annots[ 'w_upos'   ].append( w.upos  )
                annots[ 'w_xpos'   ].append( w.xpos  ) 
                annots[ 'w_feats'  ].append( w.feats )
                annots[ 'w_head'   ].append( w.head )
                annots[ 'w_deprel' ].append( w.deprel )

            self.curr_tok_idx += 1
            return indexed_object( myrepr = tok.text, idx = tok.id, annotations = annots )
        else:
            self.curr_sent_idx += 1
            self.curr_tok_idx = 0
            return self.next_tok()
    else:
        return None

  def next_sentence( self ):
    # stop at each sentence boundary
    if self.curr_sent_idx < len( self.curr_doc.sentences):
      self.curr_tok_idx = 0
      res = []
      tk = self.next_tok()
      while tk and (self.curr_tok_idx < len( self.curr_doc.sentences[ self.curr_sent_idx ].tokens )):
        res.append( tk )
        tk = self.next_tok()
      self.curr_sent_idx += 1
      return res
    else:
      return []

  def init_file( self ):
    self.curr_doc = self.nlp( self.curr_text )
    self.curr_sent_idx = 0
    self.curr_tok_idx =  0

  def next_file( self ):
      self.curr_file_idx += 1
      if self.curr_file_idx < len( self.file_lst ):
          with open( (self.document_dir  / self.file_lst[ self.curr_file_idx ]).with_suffix( self.txt_sufx ), 'r', encoding='utf-8' ) as fdesc: 
            self.curr_text = fdesc.read()
            fdesc.close()
          self.init_file()
          # print( 'stanza loaded file {0}'.format( self.file_lst[ self.curr_file_idx ] ))
          return True
      else:
          return False








